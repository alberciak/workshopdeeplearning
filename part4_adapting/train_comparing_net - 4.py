import numpy as np
from  keras import optimizers
import keras

import random as rn

import numpy.random as nrn

from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras import metrics


def generate_arrays_from_file(trainArr, trainLabelArr, batchSize):
    labelMap = {}
    for i in range(trainArr.shape[0]):
        labelMap.setdefault(trainLabelArr[i], []).append(i)

    while True:
        xArr = np.empty([batchSize, trainArr.shape[1]])
        yArr = np.empty([batchSize])

        # you have to prepare batch of feature vectors and labels in xArr and yArr
        # each feature vector should be created by subtracting features of 2 faces of the same person or 2 different persons
        # you should select people randomly
        # you should make batch uniform with positive and negative examples
        # features from 2 pictures of the same person should be drawn from first 500 people labels (this people have at least 4 pictures taken)
        # for positive examples (2 faces of the same person) label should be 0, otherwise label should be 1

        yield (xArr, yArr)




trainArr = np.load('data/featuresTrain.npy')
trainLabelArr = np.load('data/labelsTrain.npy')
valData = np.load('data/featuresVal.npy')
valDataLabels = np.load('data/labelsVal.npy')


print('Val data shape: {}'.format(valData.shape))

#test dataset reduction to make training faster
valData = valData[:10000]
valDataLabels = valDataLabels[:10000]
print('Val data shape: {}'.format(valData.shape))


rowSize = trainArr.shape[1]


# [Task 1]
# add 3 Dense layers
#   set layers size to row size
#   set last layer size to 1 (one neuron will respond with probability)
#   set input_dim on first layer to rowSize
#   set activation function on first 2 layers to 'relu'
#   set activation function on last layer to 'sigmoid'
model = Sequential()
model.add(Dense(rowSize, input_dim=rowSize, activation='relu'))
model.add(Dense(rowSize, activation='relu'))
model.add(Dense(1, activation='sigmoid'))

# [Task 2] choose optimizer from keras optimizers
opt = optimizers.adam()


# [Task 3]
# set proper loss function for classification
# add 'accuracy' to metrics list
model.compile(loss='binary_crossentropy',
              optimizer=opt,
              metrics=['accuracy'])

batch_size = 16

# [Task 4] Switch all callbacks on and see what will be the result in checkpoints and tensor_board_logs folders
callbacks = [
    keras.callbacks.ModelCheckpoint('checkpoints/weights.{epoch:02d}-{val_acc:.2f}.hdf5', monitor='val_acc', verbose=0, save_best_only=True, save_weights_only=False, mode='auto', period=1),
    keras.callbacks.TensorBoard(log_dir='tensor_board_logs', batch_size=batch_size, write_graph=True, write_grads=True)
]


# [Task 5] write python generator in function 'generate_arrays_from_file' that will provide batch of training examples every training iteration
history = model.fit_generator(generate_arrays_from_file(trainArr, trainLabelArr, batch_size), steps_per_epoch=300, epochs=50, validation_data=(valData, valDataLabels), callbacks=callbacks)


# [Task 6] save model to 'data/compare_net_model.hdf5'

# [Task 7 - OPTIONAL] check what can be retrived from history model after training
# use matplotlib to plot retrieved metrics

