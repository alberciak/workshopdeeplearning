# PART 3 - instructions


In this part you will be implementing program which will be able to find selected person in video stream using face detection and recognition networks.

First 6 tasks will be related to training fully connected neural network for comparing faces.
You should start with train_comparing_net.py script and implement tasks described in the code.
When you finish, you will be able to train your network for face comparison and save the model on disk.

Second 5 task are related to preparing inference application that will take a picture of a person using web camera 
and than it will compare this face with every face that will ever appear in camera on next frames.
You should follow tasks instructions in following script: 'inference_cam_with_recognition.py'.

If you have any problems with any task, you can have a look into reference scripts (e.g inference_cam_with_recognition - 3.py is the script that solves tasks 1-3)


Here you have a list of all task, however it will be easier to follow them in the code.


## 1.) Training network for face comparison

	Work with script: train_comparing_net.py

	[Task 1]
		add 3 Dense layers
		set layers size to row size
		set last layer size to 1 (one neuron will respond with probability)
		set input_dim on first layer to rowSize
		set activation function on first 2 layers to 'relu'
		set activation function on last layer to 'sigmoid'
	
	[Task 2]
		choose optimizer from keras optimizers
	
	[Task 3]
		set proper loss function for classification
		add 'accuracy' to metrics list
	
	[Task 4]
		Switch all callbacks on and see what will be the result in checkpoints and tensor_board_logs folders
	
	[Task 5]
		write python generator in function 'generate_arrays_from_file' that will provide batch of training examples every training iteration
	
	[Task 6]
		save model to 'data/compare_net_model.hdf5'
	
	[Task 7 - OPTIONAL]
		check what can be retrieved from history model after training
		use matplotlib to plot retrieved metrics


## 2.) Preaparing inference application to use trained network
	
	Work with script: inference_cam_with_recognition.py

	[Task 1]
		Load network model from 'data/compare_net_model.hdf5'
	
	[Task 2]
		replace line below with preprocessing steps:
			a) resize image to (224, 224) using cv2.resize
			b) change type to float
			c) expand dims along axis=0
			d) use utils prepared in VGGFace implementation - e.g. x = utils.preprocess_input(x, version=1)
	
	[Task 3]
		Initialize VGGFace model
		check what parameters should be used to return feature vector as result of inference
		(classification part should be excluded, global average pooling should be used at the end)
	
	[Task 4]
		Convert box normal coords ( 0.0-1.0 ) to frame coords (0-w and 0-h) and uncomment line below
	
	[Task 5]
		use recognition and prediction models
			a) preprocess image for recognition with implemented method
			b) call recognition model
			c) reshape output to linear feature vector
			d) take abs of subtruction of ref_features and current face features
			e) expand dims of result along axis=0

