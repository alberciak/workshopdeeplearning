import numpy as np
import cv2
import sys
from utils import label_map_util
from utils import visualization_utils_color as vis_util
from tfdetector import TensoflowFaceDector

from keras_vggface.vggface import VGGFace
from keras_vggface import utils
import keras.models

PATH_TO_CKPT = './model/frozen_inference_graph_face.pb'
PATH_TO_LABELS = './protos/face_label_map.pbtxt'

NUM_CLASSES = 2

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)


def preprocess_img_for_recognition(img):
# [Task 2]
# replace line below with preprocessing steps:
# a) resize image to (224, 224) using cv2.resize
# b) change type to float
# c) expand dims along axis=0
# d) use utils prepared in VGGFace implementation - e.g. x = utils.preprocess_input(x, version=1)

    x = cv2.resize(img, (224, 224), interpolation=cv2.INTER_LINEAR)
    x = x.astype(float)
    x = np.expand_dims(x, axis=0)
    x = utils.preprocess_input(x, version=1)

    return x

try:
    camID = int(sys.argv[1])
except:
    camID = sys.argv[1]

tDetector = TensoflowFaceDector(PATH_TO_CKPT)

# [Task 3
# Initialize VGGFace model
# check what parameters should be used to return feature vector as result of inference
# (classification part should be excluded, global average pooling should be used at the end)

recModel = VGGFace(include_top=False, pooling='avg')

cap = cv2.VideoCapture(camID)

while True:
    ret, refImg = cap.read() #  cam.frame
    if refImg is None:
        continue

    cv2.imshow("image", refImg)
    k = cv2.waitKey(20) & 0xff
    if k == ord('q') or k == 27 or k == 32 or k == 13:
        break




(boxes, scores, classes, num_detections) = tDetector.run(refImg)
arr = np.array(refImg)
maxInd = np.argmax(scores)
ymin, xmin, ymax, xmax = boxes[maxInd][0], boxes[maxInd][1], boxes[maxInd][2], boxes[maxInd][3]
xmin, ymin, xmax, ymax = int(xmin * arr.shape[1]), int(ymin * arr.shape[0]), int(xmax * arr.shape[1]), int(ymax * arr.shape[0])
subimage = arr[ymin: ymax, xmin: xmax]

ref_features = None


cv2.imshow('refImg', subimage)
cv2.waitKey()

if recModel is not None:
    x = preprocess_img_for_recognition(subimage)
    output = recModel.predict(x)
    ref_features = output.reshape(-1)

# [Task 1] Load network model from 'data/compare_net_model.hdf5'
comparingModel = keras.models.load_model('data/compare_net_model.hdf5')

min_score_threshold = 0.4
min_compare_score_threshold = 0.4

while True:
    ret, image = cap.read()
    if ret == 0:
        break

    (boxes, scores, classes, num_detections) = tDetector.run(image)

    compareScores = np.zeros(boxes.shape[0])
    arr = np.array(image)

    feat = None
    for i in range(0, boxes.shape[0]):
        if scores[i] > min_score_threshold:
            ymin, xmin, ymax, xmax = boxes[i][0], boxes[i][1], boxes[i][2], boxes[i][3]

# [Task 4] Convert box normal coords ( 0.0-1.0 ) to frame coords (0-w and 0-h) and uncomment line below

            #subimage = arr[ymin: ymax, xmin: xmax]

            if recModel is not None:

# [Task 5] use recognition and prediction models
#       a) preprocess image for recognition with implemented method
#       b) call recognition model
#       c) reshape output to linear feature vector
#       d) take abs of subtruction of ref_features and current face features
#       e) expand dims of result along axis=0

                if comparingModel is not None and feat is not None:
                    compareScores[i] = comparingModel.predict(feat)

    vis_util.visualize_boxes_and_labels_on_image_array(
        image,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=4,
        compare_scores= compareScores,
        min_compare_score=min_compare_score_threshold,
        min_score_thresh=min_score_threshold
    )

    cv2.imshow("Face detection", image)
    k = cv2.waitKey(1) & 0xff
    if k == ord('q') or k == 27:
        break

cap.release()
