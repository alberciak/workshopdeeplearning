from keras.models import Sequential
from keras.layers import Dense, Dropout, Input
import keras.models

def create_model(rowSize):

    print ("Row size: {}".format(rowSize))
    model = Sequential()
    model.add(Dense(rowSize, input_dim=rowSize, activation='relu'))
    # model.add(Dropout(0.3))
    model.add(Dense(rowSize, activation='relu'))
    # model.add(Dropout(0.3))
    model.add(Dense(1, activation='sigmoid'))
    #model.load_weights('C:\\workspace_exp\\faces\\weights.100-0.16.hdf5', by_name=True)

    model.load_weights('C:\\workspace_exp\\faces\\compare_net_model.hdf5')
    return model

#def load_mode(path):
 #   model = keras.models.load_model(path)
    #return
