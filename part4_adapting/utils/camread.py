import threading
import cv2

class CamRead(threading.Thread):
    def __init__(self, cam):
        threading.Thread.__init__(self)
        self.cam = cam
        self.frame = None
        # self.threadID = threadID
        # self.name = name
        # self.counter = counter
        self.working = True



    def run(self):
        cap = cv2.VideoCapture(self.cam)
        # cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
        # cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)

        while (self.working):
            ret, self.frame = cap.read()
            if ret == 0:
                break
        cap.release()

    def get_frame(self):
        return self.frame

    def stop(self):
        self.working = False
        if self.is_alive():
            self.join()