## PART 1

The aim of this exercise is to use existing neural network trained for face detection

1. Clone PART 1 repository
2. Launch 'python inference_cam_face.py 0' to run sample application that will connect to main camera in your laptop and detect all faces that will appear on video stream
3. You can also run this tool with movie file as parameter to detect faces in this video (e.g.  python inference_cam_face.py my_movie.mp4)
