# Deep Learning for developpers

---
## PART 0 Introduction **(30 min)**
---

1. Who are we? Our experience with DL, our projects, frameworks we use.
2. Who are you? Your experience in programming/languages? Experience with DL, particularily CNN? What frameworks have you used?
3. Review of test script. Troubleshooting attendees’ environment problems (if any)
4. Bringing some hype!

---
## PART 1 Hand-on-Labs: CNN fundamentals **(150 min)**
---

1.	Hand-on-Labs (Computer Vision) - *Given a photo of a person’s face (never seen before), find this person in live video recording*
	1.	Brainstorming for solution 
	2.	Convolutional network tasks:  detection, classification, regression, segmentation
	3.	Detection ** (part1_using) **
		1.	Using a pretrained model
		2.	Online resources (training data, network models)
	4.  Understanding convolution ** (part2_convolution) **
	5.	Classification ** (part3_building) **
		1.	Auxiliary case: glasses/non-glasses face
			1.	Handcrafting filters for convolution
			2.	Designing and training convolutional network
			3.	Learning diagnostics, performance metrics, overtraining, choosing validation
			4.	Convolutional network vs handcrafted convolution
			5.	Consequences of using small network for a big task (like our original one)
			6.	A quick walk through various frameworks (from tensorflow via deepLearningForJ to keras)

---
BREAK **(5 min)**
## PART 2 Hand-on-Labs: Advanced CNN **(150 min)**
---

2. Hands-on-Labs continuation
	4. Custom classification adapting available solutions ** (part4_adapting) **
		1.	Quick overview of state-of-the art architectures (from AlexNet to DenseNet)
		2.	Refining a model trained for similar task (associate face to person)
	5.	Combine detection with classification and test the solution on live recording in the class

---
## PART 3 Case studies: Input data and limitations **(30 min)**
---

3.	Case2 (Data mining) – *Given cars’ GPS tracks (of varying lengths and non-uniform density), find roundabouts*
	1.	Brainstorming for solution
	2.	Evaluation of possible handcrafted algorithms
	3.	Formulating neural network input
		1.	Defining structured input from unstructured data
		2.	Ground truth augmentation techniques
4.	Bonus: Case3 (Learning arithmetics) – *Given handwritten mathematical operation on numbers, provide the result using end-end net*
5.	Summary
	1.	Identifying problems suitable for deep-learning based solution
	2.	Step-by-step solution design process with decisions to be made at each step
6.	Feedback survey (https://www.surveymonkey.com/r/GBR2MJT). Questions and questions-related improvisations
