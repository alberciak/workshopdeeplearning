import tensorflow as tf
import numpy as np
import cv2
import os

class Loader:
    def __init__(self, path):
        f0 = os.path.join(path, '0')
        f1 = os.path.join(path, '1')
        self.paths = {
            0: [os.path.join(f0, x) for x in sorted(os.listdir(f0))],
            1: [os.path.join(f1, x) for x in sorted(os.listdir(f1))]
        }

    def load_datasets(self, batch=32, shuffle=False, split_ratio=0.9):
        ftrain, labtrain, fvalid, labvalid = self.split_filenames(split_ratio)
        self.titer, self.tn = Loader.setup_dataset(ftrain, labtrain, batch, shuffle)
        self.train_batch = self.titer.get_next()
        if len(labvalid)>0:
            self.viter, self.vn = Loader.setup_dataset(fvalid, labvalid, batch, shuffle)
            self.valid_batch = self.viter.get_next()

    def get_training_examples(self, sess):
        return sess.run(self.train_batch)

    def get_validation_examples(self, sess):
        return sess.run(self.valid_batch)

    def split_filenames(self, split_ratio):
        filenames_train = []
        labels_train = []
        filenames_valid = []
        labels_valid = []

        for ii in (0, 1):
            filenames = self.paths[ii]
            labels = np.ones(len(filenames)) * ii
            k = int(len(labels) * split_ratio)
            filenames_train.extend(filenames[0:k])
            filenames_valid.extend(filenames[k:])
            labels_train.extend(labels[0:k])
            labels_valid.extend(labels[k:])
        return filenames_train, labels_train, filenames_valid, labels_valid

    @classmethod
    def setup_dataset(cls, filenames, labels, batch, shuffle=False):
        ds = tf.data.Dataset.from_tensor_slices((filenames, labels))
        ds = ds.map(Loader._imread_function)
        if shuffle:
            ds = ds.shuffle(buffer_size=len(filenames))
        ds = ds.batch(batch)
        ds = ds.repeat()
        iter = ds.make_one_shot_iterator()
        return iter, len(labels)

    @classmethod
    def _imread_function(cls, filename, label):
        image_string = tf.read_file(filename=filename)
        image_decoded = tf.image.decode_image(image_string, 1)
        image_decoded = tf.reshape(image_decoded, shape=(200, 180, 1))
        img = tf.cast(image_decoded, dtype=tf.float32) / 255
        label = tf.one_hot(tf.cast(label, dtype=tf.uint8), 2)
        return img, label


def conv_layer(inp, filts, kernel, zoom, drop=0):
    out = inp
    # uncomment the following line to use batch normalization technique
    #out = tf.layers.batch_normalization(out)
    out = tf.layers.conv2d(
        inputs=out,
        filters=filts,
        kernel_size=[kernel, kernel],
        padding='SAME',
        activation=tf.nn.relu
    )
    out = tf.layers.max_pooling2d(
        inputs=out,
        pool_size=[zoom, zoom],
        strides=zoom
    )
    if drop>0:
        out = tf.layers.dropout(out, rate=drop)
    return out



def conn_layer(inp, n, dropout_rate=0):
    out = tf.layers.dense(
        inputs=inp,
        units=n,
        activation=tf.nn.relu
    )
    if dropout_rate>0:
        out = tf.layers.dropout(
            inputs=out,
            rate = dropout_rate
        )
    return out


def flatten(inp) :
    n = inp.get_shape()[1:].num_elements()
    return tf.reshape(inp, [-1, n]), n


def model_fn(features, labels, mode):
    use_dropout = False
    # uncomment the following line to use dropout while training.
    # use_dropout = mode==tf.estimator.ModeKeys.TRAIN
    conv_dropout = 0
    if use_dropout:
        conv_dropout = 0.8
    result = features
    result = conv_layer(result, 8, 3, 4, conv_dropout)
    result = conv_layer(result, 8, 3, 2, conv_dropout)
    result = conv_layer(result, 8, 3, 2, conv_dropout)
    result = conv_layer(result, 8, 3, 2, conv_dropout)
    result, _ = flatten(result)
    dropout_rate=0
    if use_dropout:
        dropout_rate=0.5
    result = conn_layer(result, 24, dropout_rate)
    result = conn_layer(result, 2)
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=result, labels=labels))
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.AdamOptimizer(learning_rate=1.5e-3)
        train_op = optimizer.minimize(loss=cost, global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=cost, train_op=train_op)
    preds = tf.argmax(result, axis=1)
    preds = tf.one_hot(preds, 2)
    return tf.estimator.EstimatorSpec(mode=mode, loss=cost, eval_metric_ops={"accuracy": tf.metrics.accuracy(labels, preds)})


def input_fn(folder, batch):
    loader = Loader(folder)
    loader.load_datasets(batch=batch, shuffle=True, split_ratio=1)
    features, labels = loader.train_batch
    return features, labels


def train_input_fn():
    return input_fn('data/train', 32)


def valid_input_fn():
    return input_fn('data/test', 256)


classifier = tf.estimator.Estimator(model_fn=model_fn, model_dir="workshop_test/0")
for step in range(100):
    classifier.train(input_fn=train_input_fn, steps=50)
    eval_results = classifier.evaluate(input_fn=valid_input_fn, steps=1)
    print(eval_results)