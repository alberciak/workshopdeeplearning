import tensorflow as tf
import numpy as np
import cv2
import os


class Loader:
    def __init__(self, path):
        f0 = os.path.join(path, '0')
        f1 = os.path.join(path, '1')
        self.paths = {
            0: [os.path.join(f0, x) for x in sorted(os.listdir(f0))],
            1: [os.path.join(f1, x) for x in sorted(os.listdir(f1))]
        }

    def load_datasets(self, batch=32, shuffle=False, split_ratio=0.9):
        if shuffle:
            ftrain, labtrain, fvalid, labvalid = self.split_filenames(split_ratio)
        # small hack for workshop usage
        else:
            n = int(batch / 2)
            ftrain = self.paths[1][0:n]
            labtrain = np.ones(shape=n).tolist()
            ftrain.extend(self.paths[0][0:n])
            labtrain.extend(np.zeros(shape=n))
            fvalid = ftrain
            labvalid = labtrain
        self.titer, self.tn = Loader.setup_dataset(ftrain, labtrain, batch, shuffle)
        self.viter, self.vn = Loader.setup_dataset(fvalid, labvalid, batch, shuffle)
        self.train_batch = self.titer.get_next()
        self.valid_batch = self.viter.get_next()

    def get_training_examples(self, sess):
        return sess.run(self.train_batch)

    def get_validation_examples(self, sess):
        return sess.run(self.valid_batch)

    def split_filenames(self, split_ratio):
        filenames_train = []
        labels_train = []
        filenames_valid = []
        labels_valid = []

        for ii in (0, 1):
            filenames = self.paths[ii]
            labels = np.ones(len(filenames)) * ii
            k = int(len(labels) * split_ratio)
            filenames_train.extend(filenames[0:k])
            filenames_valid.extend(filenames[k:])
            labels_train.extend(labels[0:k])
            labels_valid.extend(labels[k:])
        return filenames_train, labels_train, filenames_valid, labels_valid

    @classmethod
    def setup_dataset(cls, filenames, labels, batch, shuffle=False):
        ds = tf.data.Dataset.from_tensor_slices((filenames, labels))
        ds = ds.map(Loader._imread_function)
        if shuffle:
            ds = ds.shuffle(buffer_size=len(filenames))
        ds = ds.batch(batch)
        ds = ds.repeat()
        iter = ds.make_one_shot_iterator()
        return iter, len(labels)

    @classmethod
    def _imread_function(cls, filename, label):
        image_string = tf.read_file(filename=filename)
        image_decoded = tf.image.decode_image(image_string, 1)
        image_decoded = tf.reshape(image_decoded, shape=(200, 180, 1))
        img = tf.cast(image_decoded, dtype=tf.float32) / 255
        label = tf.one_hot(tf.cast(label, dtype=tf.uint8), 2)
        return img, label


class Shower(object):
    def __init__(self, sess):
        self.sess = sess
        self.posx = 20
        self.posy = 20

    def show_tensor(self, tensor, name, isfilter, resizing, pos=None, shift=(50,50)):
        if pos:
            self.posx = pos[0]
            self.posy = pos[1]
        tensor = self.tensor_to_2d(tensor, isfilter, resizing)
        tensor = (tensor * 255).astype(np.uint8)
        if np.sum(np.abs(tensor))>0:
            cv2.namedWindow(name)
            cv2.imshow(name, tensor)
            cv2.moveWindow(name, self.posx, self.posy)
            self.posx += int(shift[0])
            self.posy += int(shift[1])

    def tensor_to_2d(self, tensor, isfilter, resizing):
        with self.sess.as_default():
            if len(tensor.get_shape()) == 4:
                if isfilter:
                    nptensor = tf.transpose(tensor, [3, 0, 1, 2])
                    nptensor = nptensor.eval()
                else:
                    nptensor = tensor.eval()
                d = 3
                [n, h, w, c] = np.array(nptensor.shape)
                h = int(h * resizing)
                w = int(w * resizing)
                mat_to_show = np.zeros(shape=(n * h + n * d, w * c + c * d))
                nnn = -d
                for nn in range(n):
                    nnn += d
                    ccc = -d
                    for cc in range(c):
                        ccc += d
                        left = cc * w + ccc
                        right = (cc + 1) * w + ccc
                        top = nn * h + nnn
                        bot = (nn + 1) * h + nnn
                        temp = nptensor[nn, :, :, cc]
                        temp = cv2.resize(temp, (0, 0), fx=resizing, fy=resizing, interpolation=0)
                        mat_to_show[top:bot, left:right] = temp
            else:
                mat_to_show = tensor.eval()
                mat_to_show = cv2.resize(mat_to_show, (0, 0), fx=resizing, fy=resizing, interpolation=0)

            cv2.normalize(mat_to_show, mat_to_show, norm_type=cv2.NORM_MINMAX)
            return mat_to_show

    def update_placeholders(self, data_dict_):
        self.data_dict = data_dict_

    def show_tensor_by_name(self, name, isfilter=False, resizing=1, pos=None, shift=(50,50)):
        try:
            t = self.sess.run(self.sess.graph.get_tensor_by_name(name), self.data_dict)
            self.show_tensor(tf.constant(t), name, isfilter, resizing, pos, shift)
        except KeyError:
            pass
        except ValueError as ex:
            print(str(ex))


def display_results(sess, data_to_feed, internal_stuff= True, only_filters = False, wait_key_pressed = True):

    shower = Shower(sess)
    shower.update_placeholders(data_to_feed)

    if only_filters:
        shower.show_tensor_by_name('edge:0', True, 20, (80, 30))
        shower.show_tensor_by_name('frame:0', True, 20, (220, 30))
        shower.show_tensor_by_name('glasses:0', True, 20, (660, 30))
        shower.show_tensor_by_name('classifier:0', True, 20, (1200, 30))
        shower.show_tensor_by_name('f5:0', True, 20, (660, 340))
    else:
        s = 0.68
        shower.show_tensor_by_name('images:0', False, s, shift=(180*s,0))
        shower.show_tensor_by_name('edge:0', True, 20, shift=(80,0))
        if internal_stuff:
            shower.show_tensor_by_name('conv:0', False, s, shift=(800 * s, 0))
            shower.show_tensor_by_name('relu:0', False, s, shift=(0, 3*180*s))
        shower.show_tensor_by_name('pool:0', False, s*10, shift=(400*s,0))
        shower.show_tensor_by_name('frame:0', True, 15, shift=(15*20,0))
        shower.show_tensor_by_name('pool_1:0', False, s*20, shift=(150,0))
        shower.show_tensor_by_name('glasses:0', True, 15, shift=(120,0))
        shower.show_tensor_by_name('pool_2:0', False, s*40, shift=(180,0))
        shower.show_tensor_by_name('classifier:0', True, 15, shift=(100,0))
        shower.show_tensor_by_name('result:0', False, 200*s, shift=(100,0))

    if wait_key_pressed:
        cv2.waitKey()
    else:
        cv2.waitKey(1000)


def print_metrics(session, accuracy=None, cost=None, data_to_feed=None, validation_data_to_feed=None):

    if accuracy == None:
        return

    train_acc = session.run(accuracy, feed_dict=data_to_feed)
    train_cost = session.run(cost, feed_dict=data_to_feed)
    if validation_data_to_feed:

        valid_cost = session.run(cost, feed_dict=validation_data_to_feed)
        valid_acc = session.run(accuracy, feed_dict=validation_data_to_feed)
        global steps, size_training, batch
        epoch = int(steps / int(size_training / batch))
        msg = "{0}, acc: {1:>6.1%}, cost: {2:.3f}, val_acc: {3:>6.1%}, val_cost: {4:.3f}"
        print(msg.format(epoch + 1, train_acc, train_cost, valid_acc, valid_cost))
    else:
        msg = "acc: {0:>6.1%}, cost: {1:.3f}"
        print(msg.format(train_acc, train_cost))


def calculate_metrics(result, labels):
    try:
        probabilities = tf.nn.softmax(result, name='result')
    except:
        return None, None
    prediction = tf.argmax(probabilities, axis=1)
    the_truth = tf.argmax(labels_placeholder, axis=1)
    pred_is_correct = tf.equal(prediction, the_truth)
    accuracy = tf.reduce_mean(tf.cast(pred_is_correct, tf.float32))
    loss = tf.nn.softmax_cross_entropy_with_logits_v2(logits=result, labels=labels_placeholder)
    cost = tf.reduce_mean(loss)
    return accuracy, cost


class Filters:
    @classmethod
    def normalize(cls, tensor, low_pass=False):
        tensor = tensor.copy()
        if not low_pass:
            tensor -= np.mean(tensor)
        normL1 = np.sum(np.abs(tensor)) / tensor.size
        if normL1>0:
            tensor = tensor / normL1
        return tensor

    @classmethod
    def edge(cls, name):
        tensor = np.zeros(shape=(3, 3, 1, 4))
        edge = np.array([[2, 1, 0],
                         [2, 1, 0],
                         [2, 1, 0]])
        tensor[:, :, 0, 0] = edge
        tensor[:, :, 0, 1] = np.fliplr(edge)
        tensor[:, :, 0, 2] = np.transpose(edge)
        tensor[:, :, 0, 3] = np.flipud(np.transpose(edge))
        tensor = Filters.normalize(tensor)
        return tf.constant(tensor, dtype=tf.float32, name='edge')

    @classmethod
    def frame(cls, name):
        tensor = np.zeros(shape=(5, 5, 4, 1))
        o = [0, 0, 0, 0]
        v = [1, 1, 0, 0]
        h = [0, 0, 1, 1]
        frame = np.array([[o, h, h, h, o],
                          [v, o, o, o, v],
                          [v, o, o, o, v],
                          [v, o, o, o, v],
                          [o, h, h, h, o]])
        tensor[:, :, :, 0] = frame
        tensor = Filters.normalize(tensor)
        return tf.constant(tensor, dtype=tf.float32, name='frame')

    @classmethod
    def glasses(cls, name):
        tensor = np.zeros(shape=(6, 6, 1, 1))
        glasses = np.array([[0, 0, 0, 0, 0, 0],
                            [0, 0, 0, 0, 0, 0],
                            [0, 1, 0, 0, 1, 0],
                            [0, 0, 0, 0, 0, 0],
                            [0, 0, 0, 0, 0, 0],
                            [0, 0, 0, 0, 0, 0]])
        tensor[:, :, 0, 0] = glasses
        tensor = Filters.normalize(tensor)
        return tf.constant(tensor, dtype=tf.float32, name='glasses')

    @classmethod
    def classifier(cls, name):
        tensor = np.zeros(shape=(5, 5, 1, 2))
        yes = np.array([[0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0],
                        [0, 1, 1, 1, 0],
                        [0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0]]) / 3
        no = np.array([[1, 1, 1, 1, 1],
                       [1, 1, 1, 1, 1],
                       [1, 0, 0, 0, 1],
                       [1, 1, 1, 1, 1],
                       [1, 1, 1, 1, 1]]) / 22
        tensor[:, :, 0, 0] = no * 2
        tensor[:, :, 0, 1] = yes
        return tf.constant(tensor, dtype=tf.float32, name=name)


### build the graph
images_placeholder = tf.placeholder(tf.float32, shape=[None, 200, 180, 1], name="images")
labels_placeholder = tf.placeholder(tf.uint8, shape=[None, 2], name="labels")

edge_detector = Filters.edge(name='edge')
frame_detector = Filters.frame(name='frame')
glasses_detector = Filters.glasses(name='glasses')
glass_classifier = Filters.classifier(name='classifier')

result = images_placeholder


def filter_and_zoomout(inp, filt, zoom):
    conv = tf.nn.conv2d(inp, filt, strides=[1, 1, 1, 1], padding='SAME', name='conv')
    relu = tf.nn.relu(conv, name='relu')
    pool = tf.nn.max_pool(relu, ksize=[1, zoom, zoom, 1], strides=[1, zoom, zoom, 1], padding='SAME', name='pool')
    return pool

result = filter_and_zoomout(inp=result, filt=edge_detector, zoom=10)
result = filter_and_zoomout(inp=result, filt=frame_detector, zoom=2)
result = filter_and_zoomout(inp=result, filt=glasses_detector, zoom=2)


def classify(inp, filt):
    n = inp.get_shape()[1:].num_elements()
    classes = filt.get_shape()[3]
    inp_flat = tf.reshape(inp, [-1, n])
    filt_flat = tf.reshape(glass_classifier, [n, classes])
    out = tf.matmul(inp_flat, filt_flat, name='result')
    return out


result = classify(inp=result, filt=glass_classifier)
accuracy, cost = calculate_metrics(result, labels_placeholder)

### load data
loader = Loader('data/train')
batch = 6
loader.load_datasets(batch=batch)

### run the graph
sess = tf.Session()
imgs, labs = loader.get_training_examples(sess)
train_dict = {images_placeholder: imgs, labels_placeholder: labs}

### show results
print_metrics(sess, accuracy, cost, data_to_feed=train_dict)
display_results(sess, data_to_feed=train_dict, internal_stuff=False)