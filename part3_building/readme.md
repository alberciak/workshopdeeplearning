# Glasses detection #

The aim of this exercise is to:

1. Understand the foundations of Convolutional Neural Networks
2. Learn how to build a network with Tensorflow and other popular frameworks
3. Learn how to train network, identify and solve most common problems in training

## Python and tensorflow

First We will write python code using tensorflow. We will try to write our own hand-made classifier of faces into glasses/non-glasses cathegories. Follow the instructions below. If you get lost, in the brackets at the beginning of every point you have a checkpoint file, to which you can compare.

0. Unzip data.zip (you can browse throught the folders inside to see whats there)
1. Open step0a.py. Do not implement anything. Just have a look. It contains a draft of our solution. In the next steps we will gradually make this draft into a working solution. For a quick start we will already jump to step0b.py where most of the trivial stuff like loading files, showing images on the screen, and printing results is already implemented. In their current state, these implementations contain a lot of potentially unnecessary code, which we will remove later on. Now, we need it to help us understand everything step by step.
2. (checkpoint: step0b.py) Filter_and_zoomout function will filter the input by convolving it with the filter and then zoom out by max-pooling. First implement filtering by using tf.nn.conv2d and return the output of convolution. Before you run the program you need to modify Filters.edge function to contain 4 filters to detect left, right, bottom and top edge. Experiment by filling 3x3 edge with some values (for experimentation, you can comment out the line 227 normalizing the values) and running the program.
3. (checkpoint: step1a.py) Having filtered the edges, you will see the output channels for left and right and top and bottom will be symmetric. You can change that by adding tf.nn.relu function to operate on the result of your convolution inside filter_and_zoomout. Check the result by running the program. Finally add tf.nn.max_pool operation to the result of relu and check the result.
4. (checkpoint: step1b.py) Now implement the Filters.frame function to return a frame detector. Typical frames are rectangles, so our detector could be a 5x5 rectangle composed of horizontal and vertical edges. Remember we are now operating on 4 channel input. Experiment with the filter. When done, you can set internal_stuff=False when running display_results function at the end of your program, to limit images to be shown.
5. (checkpoint: step2.py) Implement the glasses filter
6. (checkpoint: step3.py) Implement glasses classifier. "Yes" filter should calculate the mean of 3 center pixels, "No" filter - the mean of everything else. Classify function is no longer working as convolution. It should perform sum of pixelwise multiplication of input by every of two filters (yes and no). You can implement it by flattening input and proper reshaping of filters and use tf.nn.matmul operation to accomplish that.
7. (checkpoint: step4.py) Check how our solution works on more examples (increase the batch)
8. (checkpoint: step5.py) Set only_filters=True in display_results to show only filters.
9. (checkpoint: step6.py) Prepare your code for automatic optimization:

	1. Replace all the filters by tf.Variable filled with random values from normal distribution with stddev=0.05 (tf.truncated_normal) having the same shapes as original filters
	2. Add optimizer (tf.train.AdamOptimizer) and call minimize(cost) on it. AdamOptimizer is a modified Stochastic Gradient Descent Optimizer. It will modify weights of randomly initialized filters to minimize the defined cost on the training examples on which you will run the graph.
	3. Use session to run optimizer in a loop (eg 5000 times) each time feeding placeholders with new data loaded using get_training_examples on our loader.
	4. Print metrics and display results on every training epoch. In print_metrics remember to also feed the placeholders with validation data loaded using get_validation_examples on our loader.
	5. In display_results set wait_key_pressed to false so that the loop will run continuously.

10. (checkpoint: step7.py) Play with the network by adding a layer, increasing nr of filters, adding biases etc.
11. (checkpoint: step8.py) Add a fully connected layer to your network.
11. (checkpoint: step9a.py) Add dropout to the hidden fully connected layer
12. (checkpoint: step9b.py) It is possible to write the network using high-level api. Check the result in step9c.py. Play with dropout, batch-norm by uncommenting the proper lines in the file.
13. (checkpoint: step9c.py) High-level api automatically stores the tensorflow model and the training progress in the folder specified in the Estimator constructor. You can analyze it visually in a browser using tensorboard. It is easy. In commandline, just run:
* [your-python-home]/scripts/tensorboard --logdir [Estimator-folder]*
and in your browser go to: localhost:6006

## Java and DeepLearning4J

1. Create a new maven project
2. Add dependencies to pom.xml:

    <dependencies>
        <dependency>
            <groupId>org.deeplearning4j</groupId>
            <artifactId>deeplearning4j-core</artifactId>
            <version>0.9.1</version>
        </dependency>
        <dependency>
            <groupId>org.nd4j</groupId>
            <artifactId>nd4j-native-platform</artifactId>
            <version>0.9.1</version>
        </dependency>
    </dependencies>
	
3. Create Java class with main method.
4. Create a new MultiLayerNetwork. (you might need to reimport maven dependencies - right-click your project -> maven -> reimport)
5. MultiLayerNetwork needs configuration. Create it using NeuralNetConfiguration.Builder()
	1. Use builder to add .list() of a few .layer()'s. Add a few convolutional layers (created using ConvolutionLayer.Builder) interleaved with subsampling layers (created with SubsamplingLayer.Builder) and followed by a dense layer (created using DenseLayer.Builder()) and output layer (created using OutputLayer.Builder) responsible for loss calculation. While createing layers using builders set proper kernel sizes, strides, activation functions, number of filters, pooling types. In output layer you can set the loss function to LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD.
	2. Use .setInputType(InputType.convolutionalFlat(hight, width, channels)) on configuration builder to make the model calculate number of inputs on its own.
	3. Set .learningRate(.001)
6. Set up your training and test dataset iterators. For that, use RecordReaderDataSetIterator constructed with ImageRecordReader and initialized on FileSplit.
7. initialize network (network.init()), you will train it in a loop using network.fit(data) where data is your training DataSetIterator and evaluate it using network.evaluate(test_data) where test_data is your test DataSetIterator.
8. You can verify your work against Glasses.java and pom.xml files in the repository.