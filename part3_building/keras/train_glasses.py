from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, GlobalAveragePooling2D
from keras.layers import Activation, Dropout, Flatten, Dense, BatchNormalization, Softmax
import keras.optimizers

from datetime import datetime


batch_size = 64

#1 Try to train the network with this really shallow architecture
#2 Add more convolutional and pooling layers to the network
#3 Add dropout
#4 Add regularization to your training procedure
#5 Try to switch your network to fully convolutional architecture.
	# - remove fully connected part
	# - add more modules consist of batch normalization, convolution, activation and max pooling

train_datagen = ImageDataGenerator(
        rescale=1./255
        #shear_range=0.2,
        #zoom_range=0.2,
        #horizontal_flip=True
     )

test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory(
        '../data/train',  # this is the target directory
        target_size=(200, 180),  # all images will be resized to 150x150
        batch_size=batch_size
)

validation_generator = test_datagen.flow_from_directory(
        '../data/test',
        target_size=(200, 180),
        batch_size=batch_size
)

model = Sequential()
model.add(Conv2D(8, (5, 5), input_shape=(200, 180, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(8, 8)))

model.add(Conv2D(8, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(8, 8)))

model.add(Flatten())
model.add(Dense(16))
model.add(Activation('relu'))
model.add(Dense(2))
model.add(Activation('softmax'))

opt = keras.optimizers.Adadelta()

model.compile(loss='binary_crossentropy',
              optimizer=opt,
              metrics=['accuracy'])

now = datetime.now()
logdir = "tensor_board_logs/" + now.strftime("%Y%m%d-%H%M%S") + "/"

model.fit_generator(
        train_generator,
        steps_per_epoch=(1237 * 2) / batch_size,
        epochs=20,
        validation_data=validation_generator,
        validation_steps=(138 * 2) / batch_size,
        callbacks=[keras.callbacks.TensorBoard(log_dir=logdir, batch_size=batch_size, write_graph=True, write_grads=True)])

