#!/bin/bash

# python
sudo apt-get install -y python3
sudo apt update
sudo apt-get install -y python3-pip
sudo apt-get install -y python3-dev

#.bashrc
echo "alias python=python3" >> /home/vagrant/.bashrc
echo "alias pip=pip3" >> /home/vagrant/.bashrc

#pip3 install --upgrade setuptools
#pip3 install --upgrade pip
sudo pip3 install opencv-python==3.4.1.15
sudo pip3 install numpy
sudo pip3 install scipy
sudo pip3 install matplotlib
sudo pip3 install tensorflow
sudo pip3 install keras
cd /workshop/keras_vggface
sudo pip3 install keras_vggface