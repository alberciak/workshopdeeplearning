#!/bin/bash



#WORK_DIR=/home/vagrant/setup
#
#INTELLIJ_SRC=$WORK_DIR/intellij
#PYCHARM_SRC=$WORK_DIR/pycharm
##wget -P $INTELLIJ_SRC https://data.services.jetbrains.com/products/download?platform=linux&code=IIC
#wget -P $INTELLIJ_SRC https://download-cf.jetbrains.com/idea/ideaIC-2018.2.5.tar.gz
##wget -P $PYCHARM_SRC https://data.services.jetbrains.com/products/download?platform=linux&code=PCC
#wget -P $PYCHARM_SRC https://download-cf.jetbrains.com/python/pycharm-community-2018.2.4.tar.gz
#
#cd $INTELLIJ_SRC
#tar zfx ideaIC-2018.2.5.tar.gz
#sudo ln -s $INTELLIJ_SRC/idea-IC-182.4892.20/bin/idea.sh /usr/local/bin/idea
#
#cd $PYCHARM_SRC
#tar zfx pycharm-community-2018.2.4.tar.gz
#sudo ln -s $PYCHARM_SRC/pycharm-community-2018.2.4/bin/pycharm.sh /usr/local/bin/pycharm

sudo snap install intellij-idea-community --classic --edge
sudo snap install pycharm-community --classic