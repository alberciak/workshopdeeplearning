#!/bin/bash

sudo apt-get install -y ubuntu-desktop
sudo apt-get install -y firefox

gsettings set org.gnome.desktop.session idle-delay 0
gsettings set org.gnome.desktop.screensaver lock-enabled false
sudo update-locale LANG=en_US.UTF-8 LANGUAGE="en_US:en"