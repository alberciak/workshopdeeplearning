## PART 0

In order to save your time during the workshop, we advise you to complete the most download/time critical part before the workshop.
First of all download/clone this repository.

Then you can take either of two paths:

1.	If you want to work on your machine go through the steps in part0_environment/python_preparation.txt and then java_preparation.txt
2.	If you want to work on virtual machine go through steps in part0_environment/vagrant_preparation.txt
